# Introduction
This repository contains common configuration and settings for use in W3D Hub projects

# Code Analysis
To use the common code analysis settings add the following to the `directory.build.props` file or to the `.csproj` for an individual project;

```
<Import Project="[PathToThisDirectory]/CodeAnalysis.props">
```
